﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private DatabaseContext db;

        public HomeController(DatabaseContext context)
        {
            db = context;
        }
        public IActionResult Index()
        {
            if (!HttpContext.Request.Cookies.ContainsKey("user"))
                return RedirectToAction("Login", "Account");
            ViewData["Users"] = db.Users.ToList();
            ViewData["Articles"] = db.Articles.ToList();
            return View();
        }

        /*public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        public IActionResult CreateBlog()
        {
            ViewData["Message"] = "Here u can add a new blog.";
     
            return View();
        }*/
    }
}
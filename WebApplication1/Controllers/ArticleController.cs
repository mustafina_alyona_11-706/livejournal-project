using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ArticleController : Controller
    {
        private DatabaseContext db;

        public ArticleController(DatabaseContext context)
        {
            db = context;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new Article());
        }

        [HttpPost]
        public IActionResult Create(Article article)
        {
            if (ModelState.IsValid)
            {
                db.Articles.Add(new Article
                {
                    Author = db.Users.ToList().First(u => u.Login == HttpContext.Request.Cookies["user"]),
                    AuthorId = db.Users.ToList().First(u => u.Login == HttpContext.Request.Cookies["user"]).Id,
                    Text = article.Text,
                    Title = article.Title
                });
                db.SaveChanges();
            }
            return View(new Article());
        }
    }
}
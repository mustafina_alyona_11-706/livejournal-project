using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AccountController : Controller
    {
        private DatabaseContext db;

        public AccountController(DatabaseContext context)
        {
            db = context;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View(new User());
        }

        [HttpPost]
        public IActionResult Register(User User)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Login = User.Login, FirstName = User.FirstName, LastName = User.LastName, Password = User.Password
                };
                
                db.Users.Add(user);
                
                db.SaveChanges();

                return RedirectToAction("Login");
            }
            else
                return View(new User());

        }

        public IActionResult Login()
        {
            return View(new User());
        }

        [HttpPost]
        public IActionResult Login(User User)
        {
            if (ModelState.IsValid)
            {
                User user;

                if (db.Users.ToList().Exists(u => u.Login == User.Login))
                {
                    user = db.Users.ToList().First(u => u.Login == User.Login);
                    if (user.Password.Equals(User.Password))
                    {
                        HttpContext.Response.Cookies.Append("user", User.Login,new CookieOptions{Expires = DateTimeOffset.Now.AddMinutes(25)});
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return View(new User()); //здесь везде где новые вьюшки возвращаются мы позже будем вовзращать
                            // разные представления ошибок 
                    }
                }               
                else
                {
                    return View(new User());
                }
            }
            else
            {
                return View(new User());
            }
        }
    }
}
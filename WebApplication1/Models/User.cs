﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;

namespace WebApplication1.Models
{
    public class User
    {
        
        public int Id { get; set; }
        
        //[Required]
        public string FirstName { get; set; }
        
        //[Required]
        public string LastName { get; set; }
        
        //[Required]
        //[UniqueConstraint()]
        public string Login { get; set; }
        
        //[Required]
        //[DataType(DataType.Password)]
        public string Password { get; set; }
         
        //[Required]
        //[Compare("Password", ErrorMessage = "Пароли не совпадают")]
        //[DataType(DataType.Password)]
        //public string PasswordConfirm { get; set; }
        
    }
}

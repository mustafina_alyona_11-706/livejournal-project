using WebApplication1.Models;
using System.Linq;

namespace WebApplication1
{
    public class SampleData
    {
        public static void Initialize(DatabaseContext context)
        {
            /*if (!context.Users.Any())
            {
                context.Users.AddRange(
                    new User
                    {
                        FirstName = "Alyona",
                        LastName = "Mustafina",
                        Login = "alyona"
                    },
                    new User
                    {
                        FirstName = "Ivan",
                        LastName = "Ivanov",
                        Login = "ivan666"
                    }
                );
                
                context.SaveChanges();
                
                context.Articles.AddRange(
                    new Article
                    {
                        Title = "Test Title",
                        Text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.",
                        Author = context.Users.First(),
                        AuthorId = context.Users.First().Id                                                    
                    },
                    new Article
                    {
                        Title = "Test2",
                        Text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.",
                        Author = context.Users.First(),
                        AuthorId = context.Users.First().Id
                    });

                context.SaveChanges();*/
            
            /*if (context.Articles.Any())
            {
                foreach (var u in context.Articles.ToList())
                    context.Articles.Remove(u);
                //context.Articles.RemoveRange(context.Articles.First(), context.Articles.Last());
            }

            context.SaveChanges();
            if (context.Users.Any()){
                foreach (var VARIABLE in context.Users.ToList())
                {
                    context.Users.Remove(VARIABLE);
                }
            }

            context.SaveChanges();*/
        }
    }
}